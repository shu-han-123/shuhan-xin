// index.js
// 获取应用实例
const app = getApp()
Page({
  sent(){
    wx.navigateTo({
      url: '/pages/movement/movement?id=1',
    })
  },
  data: {
    demo1:{
      d1:"每日福利",
      d2:"营地商城",
      d3:"观战中心",
      d4:"钱包/订单",
      d5:"身份认证",
    },
    fabu:{
      title:"我的发布",d1:"资讯", d2:"短视",
      d3:"动态", d4:"帖子", num1:"0",
      num2:"0",num3:"1", num4:"0",
      sent:"sent"
    },
    dingyue:{
      title:"我的订阅",d1:"专栏", d2:"主播",
      d3:"话题", d4:"赛事", num1:"0",
      num2:"0",num3:"0",num4:"0"
    },
    guanzhu:{
      num1:"0",d1:"关注"
    },
    fans:{
      num1:"2",d1:"粉丝"
    },
    fangke:{
      num1:"634",d1:"访客"
    },
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
