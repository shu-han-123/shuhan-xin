// pages/login/login.js
Page({
  getName(e){
    // console.dir(e.detail.value)
    this.data.username=e.detail.value;
  },
  getSno(e){
    // console.dir(e.detail.value)
    this.data.Sno=e.detail.value;
  },
  login(){
    wx.request({
      url: 'http://172.20.10.5:8080/untitled/login.do',
      method:"POST",
      data:{
        username:this.data.username,
        password:this.data.Sno
      },
      header: { 
        "Content-Type": "application/x-www-form-urlencoded" 
      },
      success:(res)=>{
          console.log("success")
          console.log(res)
          console.log(res.data)
          if(res.data){
            wx.showToast({
              title: '登录成功',
              icon:"success",
              duration:2000
            }),
            wx.switchTab({
              url: '/pages/index/index',
            })
          }else{
            wx.showToast({
              title: '登录失败',
              icon:"error",
              duration:2000})
          }
      }
    })
  },
  register(){
    wx.navigateTo({
      url: '/pages/register/register',
    })
  },
  // 页面的初始数据
  data: {
    username:"",
    Sno:"",
  },

  //  生命周期函数--监听页面加载
  onLoad(options) {
  },

//  生命周期函数--监听页面初次渲染完成
  onReady() {
  },

  //  生命周期函数--监听页面显示
  onShow() {

  },

  // 生命周期函数--监听页面隐藏
  onHide() {

  },

  // 生命周期函数--监听页面卸载
  onUnload() {

  },

  // 页面相关事件处理函数--监听用户下拉动作
  onPullDownRefresh() {

  },


  //   页面上拉触底事件的处理函数
  onReachBottom() {

  },

    // 用户点击右上角分享
  onShareAppMessage() {

  }
})