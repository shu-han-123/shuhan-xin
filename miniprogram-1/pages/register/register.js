// pages/register/register.js
Page({
  getusername(e){
    // console.dir(e.detail.value)
    this.data.username=e.detail.value;
  },
  getpassword(e){
    // console.dir(e.detail.value)
    this.data.password=e.detail.value;
  },
  getpasswordagain(e){
    // console.dir(e.detail.value)
    this.data.passwordagain=e.detail.value;
  },
  SureRegister(){
    if((this.data.password!=this.data.passwordagain) || this.data.password=="" ||this.data.username==""){
      console.log(this.data.password)
      console.log(this.data.passwordagain)
      wx.showToast({
        title: '两次密码不一致',
        icon:"error",
        duration:2000
      })
    }else{
      wx.request({
        url: 'http://172.20.10.5:8080/untitled/register.do',
        method:"POST",
       data:{
        username:this.data.username,
        password:this.data.password
      },
      header: { 
        "Content-Type": "application/x-www-form-urlencoded" 
      },
      success:(res)=>{
        console.log("success")
        console.log(res)
        console.log(this.data.password),
        wx.showToast({
          title: '注册成功',
          icon:"success",
          duration:2000
        })
      }
      })
    }
  },
  
  /**
   * 页面的初始数据
   */
  data: {
    username:"",
    password:"",
    passwordagain:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})