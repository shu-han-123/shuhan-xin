$(function () {                     				 //专题页面滑动效果
	let setX = 0
	$(".topicTop").on('touchstart', function (e) {
		let startX = e.originalEvent.targetTouches[0].clientX - setX
		// console.log("上：" + setX)
		$(".topicTop>div").on("touchmove", function (e) {
			let newX = e.originalEvent.touches[0].clientX
			setX = (newX - startX)
			if (setX > 0) {
				setX = 0
			}
			if (setX <= -(window.screen.width) * 1.25) {
				setX = -((window.screen.width) * 1.25)
			}
			// console.log("下：" + setX)
			$(".topicTop").css("left", setX)
		})
	})
})

$(function () {           	           	 		 //点击切换主页面
	let bottomHtml = ""
	let bottomList = [
		{ id1: "shouye1", imgsrc: "img/shouYe.png", id2: "shouye2", title: "首页" },
		{ id1: "fenlei1", imgsrc: "img/fenLei.png", id2: "fenlei2", title: "分类" },
		{ id1: "zhuanti1", imgsrc: "img/zhuanTi.png", id2: "zhuanti2", title: "专题" },
		{ id1: "wode1", imgsrc: "img/woDe.png", id2: "wode2", title: "我的" }
	]
	bottomList.forEach(button => {
		bottomHtml += `<div>
						<div id="${button.id1}">
						<img src="${button.imgsrc}" id="${button.id2}"/>
						</div>
						<span>${button.title}</span>
					</div>`
	})
	$(".bottom").html(bottomHtml)
	var list = ["#shouye", "#fenlei", "#zhuanti", "#wode"]
	var len = list.length
	$("#shouye1").addClass("intercept")
	$("#shouye2").addClass("pictureProjection")
	$(".bottom>div").eq(0).addClass("font")
	$("#index>div").eq(0).slideDown("slow")
	for (let i = 0; i < len; i++) {
		if (i != 0) {
			$("#index>div").eq(i).slideUp()
		}
	}
	function implement(index, object) {
		$(".bottom>div").eq(index).click(function () {
			$(this).addClass("font")
			$("#index>div").eq(index).slideDown("slow")
			$("#" + object + "1").addClass("intercept")
			$("#" + object + "2").addClass("pictureProjection")
			for (let i = 0; i < len; i++) {
				if (list[i] != "#" + object) {
					$(list[i] + "1").removeClass("intercept")
					$(list[i] + "2").removeClass("pictureProjection")
				}
				if (i != index) {
					$(".bottom>div").eq(i).removeClass("font")
					$("#index>div").eq(i).slideUp("slow")
				}
			}
		})
	}
	implement(0, "shouye")
	implement(1, "fenlei")
	implement(2, "zhuanti")
	implement(3, "wode")
});

$(function () {        					 //topic触摸滑动效果html
	let topicTopHtml = ""
	let topicList = [{ topic: "全部专题" }, { topic: "服装专题" }, { topic: "餐厨专题" },
	{ topic: "配件专题" }, { topic: "洗护专题" }, { topic: "居家专题" }
	]
	topicList.forEach(zhuanti => {
		topicTopHtml += `<div>
								<div>
									<img src="img/grayPicture.png">
								</div>
								<span>${zhuanti.topic}</span>
							</div>`
	})
	$(".topicTop").html(topicTopHtml)
})

$(function () {							  //topic主要内容html
	let templateTopicList = [
		{ topicname: "餐厨专题", price: "99", love: "100", eyed: "10000", message: "100" },
		{ topicname: "服装专题", price: "100", love: "200", eyed: "1000", message: "1030" },
		{ topicname: "配件专题", price: "199", love: "100", eyed: "10000", message: "100" },
		{ topicname: "电子专题", price: "6999", love: "500", eyed: "10550", message: "10320" },
		{ topicname: "电子专题", price: "6999", love: "500", eyed: "10550", message: "10320" },
		{ topicname: "电子专题", price: "6999", love: "500", eyed: "10550", message: "10320" },
		{ topicname: "电子专题", price: "6999", love: "500", eyed: "10550", message: "10320" },
		{ topicname: "电子专题", price: "6999", love: "500", eyed: "10550", message: "10320" },
	]
	let topicContent = ""
	templateTopicList.forEach(item => {
		topicContent += `<div class="templateTopic">
							<div class="template1">
								<div>
								   <div><img src="img/grayPicture.png"></div>
								</div>
								<span>${item.topicname}</span>
							</div>
							<div class="template2">
								 <div>
									<img src="img/grayPicture.png">
								</div>
								<div>
									<div>
										<span>热门</span>
									</div>
								</div>
							</div>
							<div class="template3">
								<div>五个春天的生活新题案<span>餐厨起居好物</span></div>
								<div>¥${item.price}元起</div>
							</div>
							<div class="template4">
								<div><img src="img/grayHeart.png"> &nbsp; &nbsp;${item.love}<span></span><img src="img/eye.png"> &nbsp; &nbsp;${item.eyed}</div>
								<div><img src="img/message.png"> &nbsp; &nbsp;${item.message}<span></span><img src="img/grayjiantou.png"></div>
							</div>
						</div>`
	})
	$(".topicContent").html(topicContent)
})

$(function () {              				//分类页面按钮模版
	let LeftList = [
		{ module: "服装" }, { module: "餐厨" }, { module: "配件" }, { module: "居家" },
		{ module: "洗护" }, { module: "婴童" }, { module: "杂货" }, { module: "饮食" }
	]
	let i = 1
	let LeftHtml = ""
	LeftList.forEach(item => {
		LeftHtml += `<div id="module${i}"><div>${item.module}</div></div>`
		i++
	})
	$(".left").html(LeftHtml)
})

$(function () {             			   	//分类页面内容html
	let rightList = [
		{ m1: "内裤", m2: "袜子", m3: "家居服", m4: "外套", m5: "内衣", m6: "大衣", m7: "衬衫" },
		{ m1: "锅具", m2: "功能厨具", m3: "杯壶", m4: "刀铲", m5: "餐具" },
		{ m1: "内裤", m2: "袜子", m3: "家居服", m4: "外套", m5: "内衣", m6: "大衣", m7: "衬衫" },
		{ m1: "锅具", m2: "功能厨具", m3: "杯壶", m4: "刀铲", m5: "餐具" },
		{ m1: "内裤", m2: "袜子", m3: "家居服", m4: "外套", m5: "内衣", m6: "大衣", m7: "衬衫" },
		{ m1: "锅具", m2: "功能厨具", m3: "杯壶", m4: "刀铲", m5: "餐具" },
		{ m1: "内裤", m2: "袜子", m3: "家居服", m4: "外套", m5: "内衣", m6: "大衣", m7: "衬衫" },
		{ m1: "锅具", m2: "功能厨具", m3: "杯壶", m4: "刀铲", m5: "餐具" }
	]
	let rightHtml = ""
	let i = 1
	rightList.forEach(item => {
		if (i % 2 != 0) {
			rightHtml += `<div id="moduleConcent${i}" class="moduleConcent">
									<div class="right_left">
										<div><div><img src="img/grayPicture.png"><span>${item.m1}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m2}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m3}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m4}</span></div></div>
									</div>
									<div class="rigth_right">
										<div><div><img src="img/grayPicture.png"><span>${item.m5}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m6}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m7}</span></div></div>
									</div>
								</div>`
		} else {
			rightHtml += `<div id="moduleConcent${i}" class="moduleConcent">
									<div class="right_left">
										<div><div><img src="img/grayPicture.png"><span>${item.m1}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m2}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m3}</span></div></div>
									</div>
									<div class="rigth_right">
										<div><div><img src="img/grayPicture.png"><span>${item.m4}</span></div></div>
										<div><div><img src="img/grayPicture.png"><span>${item.m5}</span></div></div>
									</div>
							   </div>`
		}
		i++
	})
	$(".right").html(rightHtml)
})

$(function () {
	//分类页面初始化
	$("#moduleConcent1").show()
	$("#module1").addClass("onclick")
	for (let i = 1; i <= 8; i++) {
		if (i != 1) {
			$("#moduleConcent" + i).hide()
		}
	}
	//分类页面控制
	function clickEvent(i) {
		$("#module" + i).click(function () {
			$(this).addClass("onclick")
			$("#moduleConcent" + i).slideDown()
			for (let j = 1; j <= 8; j++) {
				if (j != i) {
					$("#moduleConcent" + j).slideUp()
					$("#module" + j).removeClass("onclick")
				}
			}
		})

	}
	//给每个按钮绑定事件
	for (let i = 1; i <= 8; i++) {
		clickEvent(i)
	}

})

$(function(){            //首页主要内容
	let shouyeContentHtml=""
	for(i=0;i<2;i++){
		shouyeContentHtml+=`
		<div class="tuijian"><span>品牌制造商直供</span><div><span>更多推荐</span><img src="img/s_jiantou.png" alt=""></div></div>
					<div>
						<div class="modle">
							<div>
								<div class="zhizangshang">WMF制造商</div>
								<div class="shangxin"><span>9.9元起</span><div>上新</div></div>
								<div><img src="img/grayPicture.png" alt=""></div>
							</div>
							<div>
								<div class="zhizangshang">MUJI制造商</div>
								<div class="shangxin"><span>19.9元起</span><div>上新</div></div>
								<div><img src="img/grayPicture.png" alt=""></div>
							</div>
						</div>
						<div class="modle">
							<div>
								<div class="zhizangshang">WMF制造商</div>
								<div class="shangxin"><span>9.9元起</span><div>上新</div></div>
								<div><img src="img/grayPicture.png" alt=""></div>
							</div>
							<div>
								<div class="zhizangshang">MUJI制造商</div>
								<div class="shangxin"><span>19.9元起</span><div>上新</div></div>
								<div><img src="img/grayPicture.png" alt=""></div>
							</div>
						</div>
					</div>`
	}
	$(".shouyeContent").html(shouyeContentHtml)
})